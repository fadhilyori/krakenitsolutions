<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class ActivityReport extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'activity_reports';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'keterangan', 'foto_path', 'user_id'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
}
