<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\TrackSales;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AuthController extends Controller
{

    public function users(User $user)
    {
        $user = User::all();
        $params = [
            'code' => '200',
            'message' => 'User Success!',
            'data' => $user
        ];

        return response()->json($params, 200);
    }


    public function register(Request $request) 
    {
        $this->validate($request, [
            'name'      => 'required',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|min:6',
            'role'      => 'required',
        ]);

        $user = User::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => Hash::make($request->password),
            'role'      => $request->role,
            'pic'       => null,
        ]);

        $params = [
            'code' => '201',
            'message' => 'Register Success!',
            'data' => $user
        ];

        return response()->json($params, 201);
    }


    public function login(Request $request)
    {
        $email = $request->input('email');
        $password = $request->password;
        $unique_code = $request->unique_code;
        $activeUser=User::where('email', $email)->first();

        if(is_null($activeUser)){
            $params = [
                'code' => 404,
                'message' => 'Email is not Found!',
                'data' => null
            ];

            return response()->json($params, 200);
        }

        if($activeUser->role!='sales'){
            $params = [
                'code' => 401,
                'message' => 'please login as sales!',
                'data' => null
            ];

            return response()->json($params, 200);
        }

        if (!Hash::check($password, $activeUser->password)) {
            $params = [
                'code' => 401,
                'message' => 'Password is Wrong!',
                'data' => null
            ];

            return response()->json($params, 200);
        }

        if(is_null($activeUser->unique_code)) {
            $activeUser->unique_code = $unique_code;
            $activeUser->save();

            $params = [
                'code' => 200,
                'message' => 'Login Success!',
                'data' => $activeUser
            ];

            $track_sales = new TrackSales;
            $track_sales->user_id = $activeUser->id;
            $track_sales->store_id = 1;
            $track_sales->type = 'login';
            $track_sales->save();

            return response()->json($params, 200);
        }
        else {
            if($activeUser->unique_code != $unique_code){
                $params = [
                    'code' => 401,
                    'message' => 'unique_code is Wrong!',
                    'data' => null
                ];

                return response()->json($params, 200);
            }
            else{
                $activeUser->save();
                $params = [
                    'code' => 200,
                    'message' => 'Login Success!',
                    'data' => $activeUser
                ];

                $track_sales = new TrackSales;
                $track_sales->user_id = $activeUser->id;
                $track_sales->store_id = 1;
                $track_sales->type = 'login';
                $track_sales->save();

                return response()->json($params, 200);
            }
        }
    }


    public function logout(Request $request) {
        $id = $request->user_id;

        $activeUser=User::where('id', $id)->first();

        $track_sales = new TrackSales;
        $track_sales->user_id = $activeUser->id;
        $track_sales->store_id = 1;
        $track_sales->type = 'logout';
        $track_sales->save();

        $params = [
            'code' => 200,
            'message' => 'Logout Success!',
            'data' => $track_sales
        ];

        return response()->json($params, 200);
    }

}
