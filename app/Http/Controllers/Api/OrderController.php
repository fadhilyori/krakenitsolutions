<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Order;
use App\RecordSelling;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class OrderController extends Controller
{

    public function list($id){
        $order = Order::where('store_id', $id)
            ->where('confirmed', 0)->get();
        $params = [
            'code' => 200,
            'message' => 'Get Order Success!',
            'data' => $order
        ];

        return response()->json($params, 200);
    }

    public function pdf($id){
        $claim = RecordSelling::find($id);
        $order = Order::find($claim->order_id);
        $file = public_path().$order->file_path;

        return response()->download($file);
    }

}
