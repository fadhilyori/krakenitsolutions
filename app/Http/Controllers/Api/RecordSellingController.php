<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Image;
use App\RecordSelling;
use App\Order;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RecordSellingController extends Controller
{

    public function claim(Request $request){
        $this->validate($request, [
            'foto1'     => 'required',
            'foto2'     => 'required',
            'foto3'     => 'required',
            'user_id'   => 'required',
            'store_id'  => 'required',
            'order_id'  => 'required',
        ]);

        $avatar1 = $request->file('foto1');
        $avatar2 = $request->file('foto2');
        $avatar3 = $request->file('foto3');
        $filename1 = time().'(1).'.$avatar1->getClientOriginalExtension();
        Image::make($avatar1)->save(public_path('/images/product/'.$filename1));
        $filename2 = time().'(2).'.$avatar2->getClientOriginalExtension();
        Image::make($avatar2)->save(public_path('/images/product/'.$filename2));
        $filename3 = time().'(3).'.$avatar3->getClientOriginalExtension();
        Image::make($avatar3)->save(public_path('/images/product/'.$filename3));

        $record_selling = new RecordSelling;
        $record_selling->foto1 = '/images/product/'.$filename1;
        $record_selling->foto2 = '/images/product/'.$filename2;
        $record_selling->foto3 = '/images/product/'.$filename3;
        $record_selling->user_id = $request->user_id;
        $record_selling->store_id = $request->store_id;
        $record_selling->order_id = $request->order_id;
        $record_selling->save();

        $order = Order::find($request->order_id);
        $order->confirmed = 1;
        $order->save();

        $params = [
            'code' => 200,
            'message' => 'Claim Success!',
            'data' => $record_selling
        ];

        return response()->json($params, 200);
    }

}
