<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Image;
use App\Store;
use App\TrackSales;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TrackSalesController extends Controller
{

    public function scan_qr(Request $request){
        $this->validate($request, [
            'user_id'       => 'required',
            'store_id'      => 'required',
            'foto_selfie'   => 'required',
            'lat'           => 'required',
            'lng'           => 'required',
        ]);

        $store = Store::find($request->store_id);

        $slat = deg2rad($store->lat);
        $slon = deg2rad($store->lng);
        $elat = deg2rad($request->lat);
        $elon = deg2rad($request->lng);

        $dist = 6371.01 * acos(sin($slat)*sin($elat) + cos($slat)*cos($elat)*cos($slon - $elon));

        if($dist>0.05){
            $params = [
                'code' => 401,
                'message' => 'Scanning Out of Range!',
                'data' => null
            ];

            return response()->json($params, 200);            
        }

        $avatar = $request->file('foto_selfie');
        $filename = time().'.'.$avatar->getClientOriginalExtension();
        Image::make($avatar)->save(public_path('/images/selfie/'.$filename));

        $track_sales = new TrackSales;
        $track_sales->user_id = $request->user_id;
        $track_sales->store_id = $request->store_id;
        $track_sales->type = 'scan_qr';
        $track_sales->foto_selfie = '/images/selfie/'.$filename;
        $track_sales->save();

        $params = [
            'code' => 200,
            'message' => 'Scan QR Success!',
            'data' => $track_sales
        ];

        return response()->json($params, 200);
    }

}
