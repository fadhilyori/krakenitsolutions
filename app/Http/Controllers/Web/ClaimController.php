<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Http\Request;
use App\RecordSelling;
use App\Store;
use App\User;
use App\Http\Resources\WarehouseResource;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use PDF;

class ClaimController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function claim(){
        return view('claim.claim');
    }

    public function table(){
        if(Auth::user()->role=='superadmin' || Auth::user()->role=='admin'){
            $claim = RecordSelling::all();
            return DataTables::of($claim)->make(true);
        }
        else if(Auth::user()->role=='store'){
            $user = User::find(Auth::user()->id);
            $store = Store::where('user_id', $user->id)->first();
            $claim = RecordSelling::where('store_id', $store->id)->get();
            return DataTables::of($claim)->make(true);
        }
    }

    public function tableId($id){
        $claim = RecordSelling::where('store_id', $id);
        return DataTables::of($claim)->make(true);
    }

    public function viewclaim(){
        return view('claim.claim');
    }

    public function destroy($id)
    {
        if(RecordSelling::destroy($id))
        {
            redirect('/inquire/claim');
        }
        else
        {
            //pesan error
        }
    }

    public function claimpdf($id){
        $claim = RecordSelling::find($id);
        $store = Store::find($claim->store_id);
        $user = User::find($claim->user_id);
        $pdf = PDF::loadView('claim.claimpdf', compact('claim', 'store', 'user'));
        return $pdf->download('claim.pdf');
    }

    public function addclaim(Request $request)
    {
        $data =
            [
                'store_id'=> Store::all(),
                'order_id'=> Order::all()
            ];
        return view('claim.claimadd',$data);
    }

    public function saveclaim(Request $request)
    {
        $claim = new RecordSelling();
        $claim->foto1 = 'none';
        $claim->foto2 = 'none';
        $claim->foto3 = 'none';
        $claim->user_id = Auth::user()->id;
        $claim->store_id = $request->store_id;
        $claim->order_id = $request->order_id;
        if($claim->save())
        {
            return redirect('/inquiry/claim/');
        }
        else
        {
            //gagal
        }

    }

    public function verifyclaim($id){
        $claim = RecordSelling::find($id);
        $claim->status=1;
        $claim->save();
        if($claim->save()){
            return redirect('/inquiry/claim/');
        }
        else{
            //gagal
        }


    }

}
