<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Store;
use App\Order;
use App\User;
use App\OrderPart;
use App\Product;
use App\Http\Resources\WarehouseResource;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use PDF;
use Auth;

class OrderController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function order(){
        $store = Store::get();
        // return response()->json(Auth::user()->role);
        return view('order.order', compact('store'));

        //dd(Auth::user());
    }
    
    public function addOrder(Request $request){
        $order = new Order();
        $order->order_code = (string) date('YmdHis',strtotime((string) Carbon::now()));
        $order->store_id = $request->store_id;
        if (Auth::check()) {
            $order->user_id = Auth::id();
        }
        $order->confirmed = false;
        $order->save();
        return redirect('/inquiry/order/view/'. $order->id);
    }

    public function vieworder($id){
        $order= Order::find($id);
        $warehouse= Product::all();
        return view('order.orderadd', compact('order', 'warehouse'));
    }

    public function adddetailorder(Request $request){
        $op = new OrderPart();
        $op->order_qty = $request->jumlah;
        $op->order_id = $request->order_id;
        $op->product_id = $request->warehouse_id;
        $op->save();

        return redirect('/inquiry/order/view/'. $request->order_id);
    }

    public function orderTable(){
        if(Auth::user()->role=='superadmin' || Auth::user()->role=='admin'){
            $order = Order::join("users", "orders.user_id", "=", "users.id")->join("stores", "orders.store_id", "=", "stores.id")->select(['orders.id as id', 'orders.order_code as order_code', 'orders.total_harga as total_harga', 'orders.file_path as file_path', 'users.id as user_id', 'users.name as user_name', 'users.email as user_email', 'users.role as user_role', 'stores.id as store_id', 'stores.name as store_name', 'orders.confirmed as confirmed', 'orders.created_at as created_at', 'orders.updated_at as updated_at']);
            return DataTables::of($order)->make(true);    
        }
        else if(Auth::user()->role=='store'){
            $user = User::find(Auth::user()->id);
            $order = Order::join("users", "orders.user_id", "=", $user->id)->join("stores", "orders.store_id", "=", "stores.id")->select(['orders.id as id', 'orders.order_code as order_code', 'orders.total_harga as total_harga', 'orders.file_path as file_path', 'users.id as user_id', 'users.name as user_name', 'users.email as user_email', 'users.role as user_role', 'stores.id as store_id', 'stores.name as store_name', 'orders.confirmed as confirmed', 'orders.created_at as created_at', 'orders.updated_at as updated_at']);
            return DataTables::of($order)->make(true);    
        }
    }

    public function orderDetailTable($id){
        $order = OrderPart::where('order_id', $id)->join("orders", "order_parts.id", "=", "orders.id")->join("products", "order_parts.product_id", "=", "products.id")->select(["order_parts.id as id", "order_parts.order_qty as order_qty", "orders.id as order_id", "orders.order_code as order_code", "products.id as product_id", "products.name as product_name", "products.harga as product_harga"]);
        return DataTables::of($order)->make(true);
    }

    public function verifyOrder($id){
        $order = Order::find($id);
        $order->confirmed = 1;

        $op = OrderPart::where('order_id', $id)->get();
        $total = 0;

        foreach ($op as $detailorder) {
            $warehouse = Product::find($detailorder->product_id);
            $stokbaru = $warehouse->available_stock - $detailorder->order_qty;    
            $warehouse->available_stock = $stokbaru;
            $total = $total + ($warehouse->harga * $detailorder->order_qty);
            $warehouse->save();
        }

        $order->total_harga= $total;
        if ($order->save()){
            return redirect('/inquiry/order/view/'. $order->id);
        }
    }

    public function deleteorder($id){
        if (Order::destroy($id)){
            return redirect('/inquiry/order/');
        }
    }

    public function deleteorderdetail($id){
        $op = OrderPart::find($id);
        $order_id = $op->order_id;
        if (OrderPart::destroy($id)){
            return redirect('/inquiry/order/view/'. $order_id);
        }
    }

    public function printpdf($id){
        $order = Order::find($id);

        if($order->confirmed != 1){
            return redirect('/inquiry/order/view/'. $id);
        }
        $op = OrderPart::where('order_id', $id)->get();
        
        $idp = $order->product_id;
        $product = Product::where('product_id', $idp);
        
        $ids = $order->store_id;
        $store = Store::find($ids);

        $pdf = PDF::loadView('order.orderpdf', compact('order', 'op', 'product', 'store'));
        return $pdf->download('invoice.pdf');
    }
}
