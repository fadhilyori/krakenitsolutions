<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Store;
use App\Http\Resources\WarehouseResource;
use Yajra\DataTables\DataTables;

class RegisteredUserController extends Controller
{

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
	}
	
	public function sales(){
		return view('registered_user.sales');
	}

	public function salestable(){
		$sales = User::all()->where('role', 'sales');
        return DataTables::of($sales)->make(true);
	}

	public function  viewsales($id){
		$sales = User::all()->where('role', 'sales')->where('id', $id);
		return $sales;
	}

	public function addsales(Request $request){
        $sales = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'pic' => 'null',
            'role' => "sales"
        ]);

        return redirect('/registered_user/sales');
	}

	public function admin(){
		return view('registered_user.admin');
	}

	public function admintable(){
		$admin = User::all()->where('role', 'admin');
        return DataTables::of($admin)->make(true);
	}

	public function  viewadmin($id){
		$admin = User::all()->where('role', 'admin')->where('id', $id);
		return $admin;
	}

	public function addadmin(Request $request){
        $sales = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'pic' => '',
            'role' => "admin"
        ]);

        return redirect('/registered_user/admin');
	}

	public function store(){
		return view('registered_user.store');
	}

	public function storetable(){
		$store = Store::all();
        return DataTables::of($store)->make(true);
	}

	public function viewstore($id){
		$store = Store::all()->where('id', $id);
		return $store;
	}
}