<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use App\TrackSales;
use App\ActivityReport;
use App\User;
use Yajra\DataTables\DataTables;
use PDF;

class SalesController extends Controller
{

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function report($id){
		return view('sales.report', [
        'id' => $id]);
	}

	public function reporttable($id){
		$data = TrackSales::where('user_id',$id)->get();
		return DataTables::of($data)->make(true);
	}

	public function dailyactivity($id){
		return view('sales.daily',['id'=>$id]);
	}

	public function dailyactivitytable($id){
		$data = ActivityReport::where('user_id',$id)->get();
		return DataTables::of($data)->make(true);
	}

	public function dailyactivitypdf($id){
		$user = User::find($id);
		$data = ActivityReport::where('user_id',$id)->get();
		$pdf = PDF::loadView('sales.dailypdf', compact('data', 'user'));
        return $pdf->download('dailyactivity.pdf');
	}
}