<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Warehouse;
use App\Http\Resources\WarehouseResource;
use Yajra\DataTables\DataTables;

class WarehouseController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('warehouse.warehouse');
        //return WarehouseResource::collection(Warehouse::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Warehouse $warehouse)
    {
        $warehouse = Warehouse::firstOrCreate(
            [
                'name' => $request->name,
                'capacity' => $request->capacity,
                'store_id' => $request->store_id,
            ]
        );
        return new WarehouseResource($warehouse);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Warehouse $warehouse)
    {
        return new WarehouseResource($warehouse);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $product->name = $request->nama_barang;
        $product->code = $request->code;
        $product->merk = $request->merk;
        $product->harga = $request->harga;
        $product->available_stock = $request->jumlah;
        $product->save();

        return redirect('/inquiry/warehouse');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Product::destroy($id))
        {
            return redirect('/inquiry/warehouse');
        }
        else
        {
            //pesan error
        }

    }

    public function warehousetable(){
        $warehouse = Product::all();
        return DataTables::of($warehouse)->make(true);
    }

    public function edit($id) {
        $warehouse=Product::find($id);

        return view('warehouse.warehouseedit', compact('warehouse'));
    }

    public function warehouseadd(Request $request)
    {
        $product = new Product();
        $product->name = $request->nama_barang;
        $product->merk = $request->merk_barang;
        $product->harga = $request->harga;
        $product->code = $request->code;
        $product->available_stock = $request->stok;
        $product->warehouse_id = 1;
        $product->save();
        return redirect('/inquiry/warehouse');
    }
}
