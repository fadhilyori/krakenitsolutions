<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Store;
use App\OrderPart;

class Order extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_code', 'total_harga', 'file_path', 'user_id', 'store_id', 'confirmed', 'created_at'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function store() {
        return $this->belongsTo(Store::class, 'store_id');
    }

    public function order_part() {
        return $this->hasMany(OrderPart::class);
    }
}
