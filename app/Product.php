<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Warehouse;

class Product extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'code', 'merk', 'harga', 'available_stock', 'warehouse_id'
    ];

    public function warehouse() {
        return $this->belongsTo(Warehouse::class, 'warehouse_id');
    }
}
