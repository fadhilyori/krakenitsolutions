<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Store;

class RecordSelling extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'record_sellings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'foto1', 'foto2', 'foto3', 'user_id', 'store_id', 'order_id'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function store() {
        return $this->belongsTo(Store::class, 'store_id');
    }

    public function order() {
        return $this->belongsTo(Order::class, 'order_id');
    }
}
