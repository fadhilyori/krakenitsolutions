<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Warehouse;
use App\TrackSales;
use App\RecordSelling;
use App\Order;
use App\User;

class Store extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'stores';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'code', 'lat', 'lng', 'user_id'
    ];

    public function warehouse() {
        return $this->hasMany(Warehouse::class);
    }

    public function track_sales() {
        return $this->hasMany(TrackSales::class);
    }

    public function record_selling() {
        return $this->hasMany(RecordSelling::class);
    }

    public function order() {
        return $this->hasMany(Order::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
