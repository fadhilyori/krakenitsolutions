<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Store;

class TrackSales extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'track_sales';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'store_id', 'type', 'foto_selfie'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function store() {
        return $this->belongsTo(Store::class, 'store_id');
    }
}