<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Store;

class Warehouse extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'warehouses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'capacity', 'store_id'
    ];

    public function store() {
        return $this->belongsTo(Store::class, 'store_id');
    }
}
