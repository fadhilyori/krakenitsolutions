<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordSellingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record_sellings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('foto1');
            $table->string('foto2');
            $table->string('foto3');
            $table->unsignedBigInteger('user_id');
            $table->unsignedInteger('store_id');
            $table->unsignedBigInteger('order_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('record_sellings');
    }
}
