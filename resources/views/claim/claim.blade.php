@extends('adminlte::page')

@section('title', 'Kraken IT Solutions - Claims')

@section('content')
    <div class="content">
        <section class="content-header">
            <h1>
                Claims Report<br>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('/')}}">
                        <i class="fa fa-dashboard"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="{{url('/inquiry/claim')}}">
                        <i class="fa fa-file-text-o"></i> Claims List
                    </a>
                </li>
            </ol>
        </section>

        <section class="content container-fluid main-content-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><b>Claims List</b></h3>
                        </div>
                        <div class="box-body" style="padding: 10px 30px">
                            <div class="row">
                            </div>
                            <br>
                            <!-- <hr style="border-style: dashed; border-width: 0.8px; border-color: gray"> -->
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-bordered table-striped table-hover" id="laporanAccount" style="width: 100%">
                                        <thead>
                                        <tr>
                                            <td width="50">No.</td>
                                            <td><center>Tanggal</center></td>
                                            <td><center>Toko</center></td>
                                            <td><center>Nama</center></td>
                                            <td><center>Order</center></td>
                                            <td><center>Foto 1</center></td>
                                            <td><center>Foto 2</center></td>
                                            <td><center>Foto 3</center></td>
                                            <td><center>Status</center></td>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop


@if(Auth::user()->role=='store')
@section('js')
    <script type="text/javascript">

        var FormData;

        $(document).ready(function() {
            var tableLaporan = $('#laporanAccount').DataTable({
                "sDom":"ltipr",
                "lengthMenu": [[10, 30, 100, 200, -1], [10, 30, 100, 200, "All"]],
                "scrollX": true,
                "scrollY": true,
                "language": {
                    "lengthMenu": "Tampil _MENU_ data per halaman",
                    "zeroRecords": "Tidak ada data yang ditemukan",
                    "info": "Halaman _PAGE_ dari _PAGES_",
                    "infoEmpty": "Data kosong",
                    "infoFiltered": "(difilter dari total _MAX_ data)",
                    "search": "Cari :",
                },
                "processing": true,
                "serverSide": true,
                "order": [],
                "ajax": {
                    "url": "<?= url('/inquiry/claim/table')?>",
                    "type": "GET",
                },
                "columnDefs": [
                    {
                        class: "text-center",
                        width: 30,
                        "targets": [0],
                        "orderable": false,
                        render: function(data, type, row, meta){
                            return meta.row+meta.settings._iDisplayStart+1
                        }
                    },
                    {
                        "targets": [1],
                        "data": "created_at",
                        width: 30,
                        "orderable": true,
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [2],
                        "data": "store_name"
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [3],
                        "data": "user_name"
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [4],
                        "data": "order_code"
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [5],
                        render: function (data, type, row, meta) {
                            return "<img style='width:100px' src='" + row['foto1'] + "'>"
                        }
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [6],
                        render: function (data, type, row, meta) {
                            return "<img style='width:100px' src='" + row['foto2'] + "'>"
                        }
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [7],
                        render: function (data, type, row, meta) {
                            return "<img style='width:100px' src='" + row['foto3'] + "'>"
                        }
                    },
                    {
                        targets: [8],
                        "sortable": false,
                        "searchable": false,
                        render: function(data, type, row, meta){
                            return "<div class='btn-group'>"+
                                "<a href='{{url("/inquiry/claim/pdf")}}/"+row["id"]+"' class='btn btn-success'>Print</a>"+
                                "</div>";
                        }
                    },
                ],
            });

            tableLaporan.draw();
        });

    </script>
@stop

@else
@section('js')
    <script type="text/javascript">

        var FormData;

        $(document).ready(function() {
            var tableLaporan = $('#laporanAccount').DataTable({
                "sDom":"ltipr",
                "lengthMenu": [[10, 30, 100, 200, -1], [10, 30, 100, 200, "All"]],
                "scrollX": true,
                "scrollY": true,
                "language": {
                    "lengthMenu": "Tampil _MENU_ data per halaman",
                    "zeroRecords": "Tidak ada data yang ditemukan",
                    "info": "Halaman _PAGE_ dari _PAGES_",
                    "infoEmpty": "Data kosong",
                    "infoFiltered": "(difilter dari total _MAX_ data)",
                    "search": "Cari :",
                },
                "processing": true,
                "serverSide": true,
                "order": [],
                "ajax": {
                    "url": "<?= url('/inquiry/claim/table')?>",
                    "type": "GET",
                },
                "columnDefs": [
                    {
                        class: "text-center",
                        width: 30,
                        "targets": [0],
                        "orderable": false,
                        render: function(data, type, row, meta){
                            return meta.row+meta.settings._iDisplayStart+1
                        }
                    },
                    {
                        "targets": [1],
                        "data": "created_at",
                        width: 30,
                        "orderable": true,
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [2],
                        "data": "store_name"
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [3],
                        "data": "user_name"
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [4],
                        "data": "order_code"
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [5],
                        render: function (data, type, row, meta) {
                            return "<img style='width:100px' src='" + row['foto1'] + "'>"
                        }
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [6],
                        render: function (data, type, row, meta) {
                            return "<img style='width:100px' src='" + row['foto2'] + "'>"
                        }
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [7],
                        render: function (data, type, row, meta) {
                            return "<img style='width:100px' src='" + row['foto3'] + "'>"
                        }
                    },
                    {
                        targets: [8],
                        "sortable": false,
                        "searchable": false,
                        render: function(data, type, row, meta){
                            return "<div class='btn-group'>"+
                                "<a href='{{url("/inquiry/claim/delete")}}/"+row["id"]+"' class='btn btn-danger'>Delete</a> "+
                                "<a href='{{url("/inquiry/claim/pdf")}}/"+row["id"]+"' class='btn btn-success'>Print</a>"+
                                "</div>";
                        }
                    },
                ],
            });

            tableLaporan.draw();
        });

    </script>
@stop


@endif