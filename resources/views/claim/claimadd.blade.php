@extends('adminlte::page')

@section('title', 'Eventq - Claim Database')

@section('content')
    <div class="content">
        <section class="content-header">
            <h1>
                Add Claim<br>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/home') }}">
                        <i class="fa fa-dashboard"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="{{ url('/admin/account') }}">
                        <i class="fa fa-building"></i> Add Claim
                    </a>
                </li>
            </ol>
        </section>

        <section class="content container-fluid main-content-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary box-centered">
                        <div class="box-body">
                            <form class="form-main form-update" action="{{ url('/inquiry/claim/save_claim/') }}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="order_id">Order id</label>
                                    <br>
                                    <div class="order_id-wrapper" style="width: 400px;">
                                        <select class="select2" name="order_id" style="width: 100%">
                                            @foreach ($order_id as $data)
                                                <option value="{{ $data->id }}">{{ $data->id }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="store_id">Order id</label>
                                    <br>
                                    <div class="store_id-wrapper" style="width: 400px;">
                                        <select class="select2" name="store_id" style="width: 100%">
                                            @foreach ($store_id as $data)
                                                <option value="{{ $data->id }}">{{ $data->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <a href="{{ url('/inquiry/claim/') }}" class="btn btn-lg btn-danger btn-flat"><i class="fa fa-trash-o"></i>&nbsp; Batal</a>
                                <button type="submit" class="btn btn-lg btn-primary btn-flat"><i class="fa fa-save"></i>&nbsp; Simpan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
