<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Invoice</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }

        body {
            margin: 0px;
        }

        * {
            font-family: Verdana, Arial, sans-serif;
        }

        a {
            color: #fff;
            text-decoration: none;
        }

        table {
            font-size: x-small;
        }

        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }

        .invoice table {
            margin: 15px;
        }

        .invoice h3 {
            margin-left: 15px;
        }

        .information {
            background-color: #60A7A6;
            color: #FFF;
        }

        .information .logo {
            margin: 5px;
        }

        .information table {
            padding: 10px;
        }
    </style>

</head>
<body>

<div class="information">
    <table width="100%">
        <tr>
            <td align="left" style="width: 40%;">
                <pre>
Toko : {{$store->name}}
Alamat : {{$store->address}}
Petugas : {{$user->name}}
<br /><br />
Tanggal : {{$claim->created_at}}
Nomer claim : {{$claim->id}}
</pre>


            </td>
            <td align="center">
                <img src="{{url('/qr-code/generate/store/')}}/{{$store->id}}" width="100">
            </td>
            <td align="right" style="width: 40%;">
            </td>
        </tr>

    </table>
</div>


<br/>

<div class="invoice">
    <h3>Claim #{{$claim->id}}</h3>
    <table width="100%">
        <thead>
        <tr>
            <th>Foto 1</th>
            <th>Foto 2</th>
            <th>Foto 3</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><img src="{{ url($claim->foto1)}}" width="200"></td>
            <td><img src="{{ url($claim->foto2)}}" width="200"></td>
            <td><img src="{{ url($claim->foto3)}}" width="200"></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </tbody>
    </table>
</div>

<div class="information" style="position: absolute; bottom: 0;">
    <table width="100%">
        <tr>
            <td align="left" style="width: 50%;">
                &copy; {{ date('Y') }} KrakenITSolutions - All rights reserved.
            </td>
            <td align="right" style="width: 50%;">
                
            </td>
        </tr>

    </table>
</div>
</body>
</html>