@extends('adminlte::page')

@section('title', 'Kraken IT Solutions - Order')

@section('content')
    <div class="content">
        <section class="content-header">
            <h1>
                Order Report<br>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('/')}}">
                        <i class="fa fa-dashboard"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="{{url('/inquiry/order')}}">
                        <i class="fa fa-file-text-o"></i> Order List
                    </a>
                </li>
            </ol>
        </section>

        <section class="content container-fluid main-content-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><b>Order List</b></h3>
                        </div>
                        <div class="box-body" style="padding: 10px 30px">
                            <div class="row">
                                <div class="col-md-12">
                                    <form action="{{ url('/inquiry/order/add') }}" method="POST">
                                        @csrf
                                        @if(Auth::user()->role!='store')
                                            <select name="store_id" required="">
                                                <option value="">pilih store</option>
                                                @foreach($store as $value)
                                                    <option value="{{$value->id}}">{{$value->id}} - {{$value->name}}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            @foreach($store as $value)
                                                @if($value->user_id == Auth::user()->id)
                                                    <select name="store_id" required="">
                                                        <option value="{{$value->id}}">{{$value->id}} - {{$value->name}}</option>
                                                    </select>
                                                @endif
                                            @endforeach
                                        @endif
                                        <input type="submit" value="Add Order" class="btn btn-primary">
                                    </form>
                                </div>
                            </div>
                            <br>
                            <!-- <hr style="border-style: dashed; border-width: 0.8px; border-color: gray"> -->
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-bordered table-striped table-hover" id="laporanAccount" style="width: 100%">
                                        <thead>
                                        <tr>
                                            <td width="50">No.</td>
                                            <td><center>Tanggal</center></td>
                                            <td><center>Person in Charge</center></td>
                                            <td><center>Toko</center></td>
                                            <td><center>Action</center></td>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @stop
@section('js')
    <script type="text/javascript">

        var FormData;

        $(document).ready(function() {
            var tableLaporan = $('#laporanAccount').DataTable({
                "sDom":"ltipr",
                "lengthMenu": [[10, 40, 100, 200, -1], [10, 30, 100, 200, "All"]],
                "scrollX": true,
                "scrollY": true,
                "language": {
                    "lengthMenu": "Tampil _MENU_ data per halaman",
                    "zeroRecords": "Tidak ada data yang ditemukan",
                    "info": "Halaman _PAGE_ dari _PAGES_",
                    "infoEmpty": "Data kosong",
                    "infoFiltered": "(difilter dari total _MAX_ data)",
                    "search": "Cari :",
                },
                "processing": true,
                "serverSide": true,
                "order": [],
                "ajax": {
                    "url": "<?= url('/inquiry/order/order_table')?>",
                    "type": "GET",
                },
                "columnDefs": [
                    {
                        class: "text-center",
                        width: 30,
                        "targets": [0],
                        "orderable": false,
                        render: function(data, type, row, meta){
                            return meta.row+meta.settings._iDisplayStart+1
                        }
                    },
                    {
                        "targets": [1],
                        "data": "tanggal",
                        width: 90,
                        "orderable": true,
                        "data": "created_at"
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [2],
                        "data": "user_name"
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [3],
                        "data": "store_name"
                    },
                    {
                        "targets": [4],
                        "sortable": false,
                        "searchable": false,
                        render: function(data, type, row, meta){
                            return "<div class='btn-group'>"+
                                "<a href='<?= url("/inquiry/order/view/")?>"+"/"+row["id"]+"' class='btn btn-success'>View</a>"+
                                "</div>";
                        }
                    },
                ],
            });

            tableLaporan.draw();
        });

    </script>
@stop