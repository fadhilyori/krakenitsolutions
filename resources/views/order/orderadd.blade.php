@extends('adminlte::page')

@section('title', 'Kraken IT Solutions - Add Order')

@section('content')
    <div class="content">
        <section class="content-header">
            @if($order->confirmed == 0)
                <h1>Add Order<br></h1>
            @else
                <h1>Order Detail<br></h1>
            @endif
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/home') }}">
                        <i class="fa fa-dashboard"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="{{ url('/inquiry/order') }}">
                        @if($order->confirmed == 0)
                            <i class="fa fa-building"></i> Add Order
                        @else
                            <i class="fa fa-building"></i> Order : {{ $order->order_code }}
                        @endif
                    </a>
                </li>
            </ol>
        </section>

        <section class="content container-fluid main-content-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary box-centered">
                        <div class="box-body">
                            <form class="form-main form-update" action="{{ url('/inquiry/order_detail/add') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="nama_barang">Tanggal</label>
                                    <input type="text" class="form-control" name="tanggal" required disabled="" value="{{Carbon\Carbon::now()}}">
                                </div>
                                <div class="form-group">
                                    <label for="nama_barang">Kode Order</label>
                                    <input type="text" class="form-control" name="order_code" required value="{{$order->order_code}}" readonly>
                                    <input type="text" name="order_id" value="{{$order->id}}" hidden>
                                </div>
                                @if($order->confirmed == 0)
                                <div class="form-group">
                                    <label for="nama_barang">Warehouse</label>
                                    <select name="warehouse_id" required="" class="form-control">
                                        <option value="">Pilih Warehouse</option>
                                        @foreach($warehouse as $value)
                                                <option value="{{$value->id}}">{{$value->code}} - {{$value->name}} - Rp. {{$value->harga}} - available : {{$value->available_stock}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="jumlah">Jumlah</label>
                                    <input type="number" class="form-control" name="jumlah" id="jumlah" placeholder="Masukkan jumlah barang" min="1" max="{{$value->available_stock}}">
                                </div>
                                <a href="{{ url('/inquiry/order') }}" class="btn btn-lg btn-danger btn-flat"><i class="fa fa-trash-o"></i>&nbsp; Batal</a>
                                <button type="submit" class="btn btn-lg btn-primary btn-flat"><i class="fa fa-save"></i>&nbsp; Simpan</button>
                                @endif
                            </form>
                            <br />
                            <br />
                            <br />
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-bordered table-striped table-hover" id="laporanAccount" style="width: 100%">
                                        <thead>
                                        <tr>
                                            <td width="50">No.</td>
                                            <td><center>Produk</center></td>
                                            <td><center>Jumlah</center></td>
                                            <td><center>Harga</center></td>
                                            @if($order->confirmed == 0)
                                                <td><center>Aksi</center></td>
                                            @endif
                                        </tr>
                                        </thead>
                                    </table>

                                    @if($order->confirmed == 0)
                                    <a href="{{ url('/inquiry/order/verify/') }}/{{$order->id}}" class="btn btn-lg btn-success btn-flat"><i class="fa fa-check"></i>&nbsp; Fix Order</a>
                                    @else
                                    <a href="{{ url('inquiry/order/pdf/') }}/{{$order->id}}" class="btn btn-lg btn-primary btn-flat"><i class="fa fa-print"></i>&nbsp; Print PDF</a>
                                    @endif
                                    <a href="{{ url('inquiry/order/delete/') }}/{{$order->id}}" class="btn btn-lg btn-danger btn-flat"><i class="fa fa-trash"></i>&nbsp; Hapus Pesanan Ini</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@if($order->confirmed == 0)
@section('js')
    <script type="text/javascript">

        var FormData;

        $(document).ready(function() {
            var tableLaporan = $('#laporanAccount').DataTable({
                "sDom":"ltipr",
                "lengthMenu": [[10, 30, 100, 200, -1], [10, 30, 100, 200, "All"]],
                "scrollX": true,
                "scrollY": true,
                "language": {
                    "lengthMenu": "Tampil _MENU_ data per halaman",
                    "zeroRecords": "Tidak ada data yang ditemukan",
                    "info": "Halaman _PAGE_ dari _PAGES_",
                    "infoEmpty": "Data kosong",
                    "infoFiltered": "(difilter dari total _MAX_ data)",
                    "search": "Cari :",
                },
                "processing": true,
                "serverSide": true,
                "order": [],
                "ajax": {
                    "url": "<?= url('/inquiry/order/table')?>/{{$order->id}}",
                    "type": "GET",
                },
                "columnDefs": [
                    {
                        class: "text-center",
                        width: 30,
                        "targets": [0],
                        "orderable": false,
                        render: function(data, type, row, meta){
                            return meta.row+meta.settings._iDisplayStart+1
                        }
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [1],
                        "data": "product_name"
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [2],
                        "data": "order_qty"
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [3],
                        "data": "product_harga"
                    },
                    {
                        "targets": [4],
                        "sortable": false,
                        "searchable": false,
                        render: function(data, type, row, meta){
                                return "<div class='btn-group'>"+
                                    "<a class='btn btn-danger' href='<?= url("/inquiry/order_detail/delete")?>/"+ row['id'] +"'>Hapus</a>"+
                                    "</div>";
                        }
                    },

                ],
            });

            tableLaporan.draw();
        });

    </script>
@stop

@else
@section('js')
    <script type="text/javascript">

        var FormData;

        $(document).ready(function() {
            var tableLaporan = $('#laporanAccount').DataTable({
                "sDom":"ltipr",
                "lengthMenu": [[10, 30, 100, 200, -1], [10, 30, 100, 200, "All"]],
                "scrollX": true,
                "scrollY": true,
                "language": {
                    "lengthMenu": "Tampil _MENU_ data per halaman",
                    "zeroRecords": "Tidak ada data yang ditemukan",
                    "info": "Halaman _PAGE_ dari _PAGES_",
                    "infoEmpty": "Data kosong",
                    "infoFiltered": "(difilter dari total _MAX_ data)",
                    "search": "Cari :",
                },
                "processing": true,
                "serverSide": true,
                "order": [],
                "ajax": {
                    "url": "<?= url('/inquiry/order/table')?>/{{$order->id}}",
                    "type": "GET",
                },
                "columnDefs": [
                    {
                        class: "text-center",
                        width: 30,
                        "targets": [0],
                        "orderable": false,
                        render: function(data, type, row, meta){
                            return meta.row+meta.settings._iDisplayStart+1
                        }
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [1],
                        "data": "product_name"
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [2],
                        "data": "order_qty"
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [3],
                        "data": "product_name"
                    },
                ],
            });

            tableLaporan.draw();
        });

    </script>
@stop

@endif