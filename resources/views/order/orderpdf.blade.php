<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Invoice</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }

        body {
            margin: 0px;
        }

        * {
            font-family: Verdana, Arial, sans-serif;
        }

        a {
            color: #fff;
            text-decoration: none;
        }

        table {
            font-size: x-small;
        }

        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }

        .invoice table {
            margin: 15px;
        }

        .invoice h3 {
            margin-left: 15px;
        }

        .information {
            background-color: #60A7A6;
            color: #FFF;
        }

        .information .logo {
            margin: 5px;
        }

        .information table {
            padding: 10px;
        }
    </style>

</head>
<body>

<div class="information">
    <table width="100%">
        <tr>
            <td align="left" style="width: 40%;">
                <pre>
Toko : {{$store->name}}
Alamat : {{$store->address}}
<br /><br />
Tanggal : {{$order->created_at}}
Nomer nota : {{$order->id}}
<br />
Status: <?php if($order->confirmed==1){ echo "Paid";} else { echo "Unconfirmed by Store and Sales";} ?>
</pre>


            </td>
            <td align="center">
                
            </td>
            <td align="right" style="width: 40%;">
                <img src="{{url('/qr-code/generate/store/')}}/{{$store->id}}" width="100">
            </td>
        </tr>

    </table>
</div>


<br/>

<div class="invoice">
    <h3>Nota #{{$order->id}}</h3>
    <table width="100%">
        <thead>
        <tr>
            <th>Merk</th>
            <th>Description</th>
            <th>Quantity</th>
            <th>Total</th>
        </tr>
        </thead>
        <tbody>
        @foreach($op as $or)
        <tr>
            <td>{{$or->product->merk}}</td>
            <td>{{$or->product->name}}</td>
            <td>{{$or->order_qty}}</td>
            <td align="left">Rp. {{ $or->product->harga * $or->order_qty}}</td>
        </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </tbody>

        <tfoot>
        <tr>
            <td colspan="1"></td>
            <td></td>
            <td align="left">Total</td>
            <td align="left" class="gray">Rp. {{$order->total_harga}}</td>
        </tr>
        </tfoot>
    </table>
</div>

<div class="information" style="position: absolute; bottom: 0;">
    <table width="100%">
        <tr>
            <td align="left" style="width: 50%;">
                &copy; {{ date('Y') }} KrakenITSolutions - All rights reserved.
            </td>
            <td align="right" style="width: 50%;">
                
            </td>
        </tr>

    </table>
</div>
</body>
</html>