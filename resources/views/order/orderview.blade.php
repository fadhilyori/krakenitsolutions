@extends('adminlte::page')

@section('title', 'Kraken IT Solutions - View Order')

@section('content')
    <div class="content">
        <section class="content-header">
            <h1>
                View Order<br>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">
                        <i class="fa fa-dashboard"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="{{ url('/inquiry/order') }}">
                        <i class="fa fa-building"></i> View Order
                    </a>
                </li>
            </ol>
        </section>

        <section class="content container-fluid main-content-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary box-centered">
                        <div class="box-body">
                                <div class="form-group">
                                    <label for="nama_barang">Tanggal Order</label>
                                    <input type="text" class="form-control" name="tanggal" required disabled="" value="{{$order->tanggal}}">
                                </div>
                                <div class="form-group">
                                    <label for="nama_barang">Nomer Order</label>
                                    <input type="text" class="form-control" name="order_id" required value="{{$order->order_code}}" readonly>
                                </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-bordered table-striped table-hover" id="laporanAccount" style="width: 100%">
                                        <thead>
                                        <tr>
                                            <td width="50">No.</td>
                                            <td><center>Warehouse</center></td>
                                            <td><center>Jumlah</center></td>
                                            <td><center>Harga</center></td>
                                            <td><center>Total</center></td>
                                            <?php if(Session::get('User')->user_role == 2 || Session::get('User')->user_role == 0){ ?><td><center>Aksi</center></td><?php } ?>
                                        </tr>
                                        </thead>
                                    </table>
                                    <a href="{{ url('inquiry/order/pdf/') }}/{{$order->id}}" class="btn btn-lg btn-primary btn-flat"><i class="fa fa-print"></i>&nbsp; Print PDF</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('js')
    <script type="text/javascript">

        var FormData;

        $(document).ready(function() {
            var tableLaporan = $('#laporanAccount').DataTable({
                "sDom":"ltipr",
                "lengthMenu": [[10, 30, 100, 200, -1], [10, 30, 100, 200, "All"]],
                "scrollX": true,
                "scrollY": true,
                "language": {
                    "lengthMenu": "Tampil _MENU_ data per halaman",
                    "zeroRecords": "Tidak ada data yang ditemukan",
                    "info": "Halaman _PAGE_ dari _PAGES_",
                    "infoEmpty": "Data kosong",
                    "infoFiltered": "(difilter dari total _MAX_ data)",
                    "search": "Cari :",
                },
                "processing": true,
                "serverSide": true,
                "order": [],
                "ajax": {
                    "url": "<?= url('/admin/store/orderdetail_datatable/')?>/{{$order->id}}",
                    "type": "GET",
                },
                "columnDefs": [
                    {
                        class: "text-center",
                        width: 30,
                        "targets": [0],
                        "orderable": false,
                        render: function(data, type, row, meta){
                            return meta.row+meta.settings._iDisplayStart+1
                        }
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [1],
                        "data": "warehouse_id"
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [2],
                        "data": "jumlah"
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [3],
                        "data": "harga"
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [4],
                        "data": "total"
                    },
                    {
                        "targets": [5],
                        "sortable": false,
                        "searchable": false,
                        render: function(data, type, row, meta){
                                return "<div class='btn-group'>"+
                                    "<a href='http://iam-zero.com/krakenitsolutions/admin/store/verify_orderdetail/"+row["id"]+"' class='btn btn-success'>Verify</a>"+
                                    "<a href='http://iam-zero.com/krakenitsolutions/admin/store/cancel_orderdetail/"+row["id"]+"' class='btn btn-warning'>Cancel</a>"+
                                    "</div>";
                        }
                    },

                ],
            });

            tableLaporan.draw();
        });

    </script>
@stop