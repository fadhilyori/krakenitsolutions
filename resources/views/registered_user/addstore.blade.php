@extends('adminlte::page')

@section('title', 'Kraken IT Solutions - Add Store')

@section('content')
    <div class="content">
        <section class="content-header">
            <h1>
                Add a Store<br>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">
                        <i class="fa fa-dashboard"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="{{ url('/registered_user/store') }}">
                        <i class="fa fa-building"></i> Add Store
                    </a>
                </li>
            </ol>
        </section>

        <section class="content container-fluid main-content-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary box-centered">
                        <div class="box-body">
                            <form class="form-main form-update" action="{{ url('/registered_user/store/add') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Nama</label>
                                    <input type="text" class="form-control" name="name" required autofocus="">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" name="email" required autofocus="">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" name="password" required autofocus="">
                                </div>
                                <div class="form-group">
                                    <label for="name">Nama Toko</label>
                                    <input type="text" class="form-control" name="storename" required autofocus="">
                                </div>
                                <div class="form-group">
                                    <label for="name">Address</label>
                                    <input type="text" class="form-control" name="address" required autofocus="">
                                </div>
                                <div class="form-group">
                                    <label for="name">Lat</label>
                                    <input type="text" class="form-control" name="lat" required autofocus="">
                                </div>
                                <div class="form-group">
                                    <label for="name">Lng</label>
                                    <input type="text" class="form-control" name="lng" required autofocus="">
                                </div>
                                <a href="{{ url('/registered_user/store') }}" class="btn btn-lg btn-danger btn-flat"><i class="fa fa-trash-o"></i>&nbsp; Batal</a>
                                <button type="submit" class="btn btn-lg btn-primary btn-flat"><i class="fa fa-save"></i>&nbsp; Simpan</button>
                            </form>
                            <br />
                            <br />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection