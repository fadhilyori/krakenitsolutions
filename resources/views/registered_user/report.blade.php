@extends('adminlte::page')

@section('title', 'Kraken IT Solutions - Sales Report')

@section('content')
<div class="content">
    <section class="content-header">
        <h1>
            Sales Report<br>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?= url('dashboard')?>">
                    <i class="fa fa-dashboard"></i> Dashboard
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-file-text-o"></i> Sales Report List
                </a>
            </li>
        </ol>
    </section>

    <section class="content container-fluid main-content-container">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><b>Sales List</b></h3>
                    </div>
                    <div class="box-body" style="padding: 10px 30px">
                        <div class="row">
                            <!-- 
<div class="col-md-12"> 
<a href="{{ url('#') }}" class="btn btn-primary">Add an Account</a> 
</div>-->
                        </div>
                        <br>
                        <!-- <hr style="border-style: dashed; border-width: 0.8px; border-color: gray"> -->
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered table-bordered table-striped table-hover" id="laporanAccount"
                                    style="width: 100%">
                                    <thead>
                                        <tr>
                                            <td width="50">No.</td>
                                            <td>
                                                <center>Nama Toko</center>
                                            </td>
                                            <td>
                                                <center>Type</center>
                                            </td>
                                            <td>
                                                <center>Dibuat Pada</center>
                                            </td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop

@section('js')
<script type="text/javascript">
    var FormData;

    $(document).ready(function () {
        var tableLaporan = $('#laporanAccount').DataTable({
            "sDom": "ltipr",
            "lengthMenu": [
                [10, 30, 100, 200, -1],
                [10, 30, 100, 200, "All"]
            ],
            "scrollX": true,
            "scrollY": true,
            "language": {
                "lengthMenu": "Tampil _MENU_ data per halaman",
                "zeroRecords": "Tidak ada data yang ditemukan",
                "info": "Halaman _PAGE_ dari _PAGES_",
                "infoEmpty": "Data kosong",
                "infoFiltered": "(difilter dari total _MAX_ data)",
                "search": "Cari :",
            },
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?= url('/sales/report/table/') ?>/{{$id}}",
                "type": "GET",
            },
            "columnDefs": [{
                    class: "text-center",
                    width: 30,
                    "targets": [0],
                    "orderable": false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1
                    }
                },
                {
                    class: "text-center",
                    "orderable": false,
                    "targets": [1],
                    "data": "store_id"
                },
                {
                    class: "text-center",
                    "orderable": true,
                    "targets": [2],
                    "data": "type"
                },
                {
                    class: "text-center",
                    "orderable": true,
                    "targets": [3],
                    "data": "created_at"
                },

            ],
        });

        tableLaporan.draw();
    });

</script>
@stop
