@extends('adminlte::page')

@section('title', 'Kraken IT Solutions - Store Database')

@section('content')
    <div class="content">
        <section class="content-header">
            <h1>
                Registered Store<br>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('/')}}">
                        <i class="fa fa-dashboard"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="{{url('/registered_user/store')}}">
                        <i class="fa fa-file-text-o"></i> Store List
                    </a>
                </li>
            </ol>
        </section>

        <section class="content container-fluid main-content-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><b>Store List</b></h3>
                        </div>
                        <div class="box-body" style="padding: 10px 30px">
                            <div class="row">
                            @if (Auth::user()->role=="superadmin")
                                <div class="col-md-12">
                                    <a href="{{ url('/registered_user/store/add') }}" class="btn btn-primary">Add a Store</a>
                                </div>
                            @endif
                            </div>
                            <br>
                            <!-- <hr style="border-style: dashed; border-width: 0.8px; border-color: gray"> -->
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-bordered table-striped table-hover" id="laporanAccount" style="width: 100%">
                                        <thead>
                                        <tr>
                                            <td width="50">No.</td>
                                            <td><center>Nama Toko</center></td>
                                            <td><center>Alamat Toko</center></td>
                                            <td><center>QR Code</center></td>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop

@section('js')
    <script type="text/javascript">

        var FormData;

        $(document).ready(function() {
            var tableLaporan = $('#laporanAccount').DataTable({
                "sDom":"ltipr",
                "lengthMenu": [[10, 30, 100, 200, -1], [10, 30, 100, 200, "All"]],
                "scrollX": true,
                "scrollY": true,
                "language": {
                    "lengthMenu": "Tampil _MENU_ data per halaman",
                    "zeroRecords": "Tidak ada data yang ditemukan",
                    "info": "Halaman _PAGE_ dari _PAGES_",
                    "infoEmpty": "Data kosong",
                    "infoFiltered": "(difilter dari total _MAX_ data)",
                    "search": "Cari :",
                },
                "processing": true,
                "serverSide": true,
                "order": [],
                "ajax": {
                    "url": "<?= url('/registered_user/store/table')?>",
                    "type": "GET",
                },
                "columnDefs": [
                    {
                        class: "text-center",
                        width: 30,
                        "targets": [0],
                        "orderable": false,
                        render: function(data, type, row, meta){
                            return meta.row+meta.settings._iDisplayStart+1
                        }
                    },
                    {
                        "targets": [1],
                        "data": "name",
                        width: 90,
                        "orderable": true,
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [2],
                        "data": "address"
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [3],
                        render: function(data, type, row, meta){
                            return "<img src='{{url("/qr-code/generate/store")}}/"+row["id"]+"' height='200' width='200'>";
                        }
                    }
                ],
            });

            tableLaporan.draw();
        });

        function hapus(){
            window.confirm('Dihapus?');
        }

    </script>
@stop