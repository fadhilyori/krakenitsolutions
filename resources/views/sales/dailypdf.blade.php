<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Daily Activity</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }

        body {
            margin: 0px;
        }

        * {
            font-family: Verdana, Arial, sans-serif;
        }

        a {
            color: #fff;
            text-decoration: none;
        }

        table {
            font-size: x-small;
        }

        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }

        .invoice table {
            margin: 15px;
        }

        .invoice h3 {
            margin-left: 15px;
        }

        .information {
            background-color: #60A7A6;
            color: #FFF;
        }

        .information .logo {
            margin: 5px;
        }

        .information table {
            padding: 10px;
        }
    </style>

</head>
<body>

<div class="information">
    <table width="100%">
        <tr>
            <td align="left" style="width: 40%;">
                <pre>
Nama : {{$user->name}}
Email: {{$user->email}}
</pre>


            </td>
            <td align="center">
                
            </td>
            <td align="right" style="width: 40%;">
            </td>
        </tr>

    </table>
</div>


<br/>

<div class="invoice">
    <h3>Daily Activity</h3>
    <table width="100%">
        <thead>
        <tr>
            <th>No</th>
            <th>Tanggal</th>
            <th>Keterangan</th>
        </tr>
        </thead>
        <tbody>
    @foreach ($data as $d)
        <tr>
            <td>{{$d->id}}</td>
            <td>{{$d->created_at}}</td>
            <td>{{$d->keterangan}}</td>
        </tr>
    @endforeach
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </tbody>
    </table>
</div>

<div class="information" style="position: absolute; bottom: 0;">
    <table width="100%">
        <tr>
            <td align="left" style="width: 50%;">
                &copy; {{ date('Y') }} KrakenITSolutions - All rights reserved.
            </td>
            <td align="right" style="width: 50%;">
                
            </td>
        </tr>

    </table>
</div>
</body>
</html>