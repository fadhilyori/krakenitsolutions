    <li class="header">DATABASE</li>
@if(Auth::user()->role=='superadmin')
    <li class="active treeview menu-open">
        <a href="#">
            <i class="fa fa-fw fa-share "></i>
            <span>Data</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="active treeview menu-open">
                <a href="#">
                    <i class="fa fa-fw fa-circle-o "></i>
                    <span>Registered User</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="">
                        <a href="{{url('/registered_user/sales')}}">
                            <i class="fa fa-fw fa-circle-o "></i>
                            <span>Sales</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{url('/registered_user/admin')}}">
                            <i class="fa fa-fw fa-circle-o "></i>
                            <span>admin</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="{{url('/registered_user/store')}}">
                            <i class="fa fa-fw fa-circle-o "></i>
                            <span>store</span>
                        </a>
                    </li>
                </ul>
            </li>
@elseif(Auth::user()->role=='admin')
    <li class="active treeview menu-open">
        <a href="#">
            <i class="fa fa-fw fa-share "></i>
            <span>Data</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="active treeview menu-open">
                <a href="#">
                    <i class="fa fa-fw fa-circle-o "></i>
                    <span>Registered User</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="">
                        <a href="{{url('/registered_user/sales')}}">
                            <i class="fa fa-fw fa-circle-o "></i>
                            <span>Sales</span>
                        </a>
                    </li>
                </ul>
            </li>
@endif
    <li class="treeview">
        <a href="#">
            <i class="fa fa-fw fa-circle-o "></i>
            <span>Inquiry</span>
                            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
                    </a>
                    <ul class="treeview-menu">
                
    <li class="">
        <a href="{{url('/inquiry/order')}}">
            <i class="fa fa-fw fa-circle-o "></i>
            <span>Order</span>
                    </a>
            </li>

    <li class="">
        <a href="{{url('/inquiry/claim')}}">
            <i class="fa fa-fw fa-circle-o "></i>
            <span>Claim</span>
                    </a>
            </li>

    <li class="">
        <a href="{{url('/inquiry/warehouse')}}">
            <i class="fa fa-fw fa-circle-o "></i>
            <span>Warehouse</span>
                    </a>
            </li>
            </ul>
            </li>
            </ul>
            </li>