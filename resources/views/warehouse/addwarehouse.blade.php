@extends('adminlte::page')

@section('title', 'krakenitsolutions - Warehouse Database')

@section('content')
    <div class="content">
        <section class="content-header">
            <h1>
                Add item to warehouse<br>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">
                        <i class="fa fa-dashboard"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="{{ url('/inquiry/warehouse') }}">
                        <i class="fa fa-building"></i> add item
                    </a>
                </li>
            </ol>
        </section>

        <section class="content container-fluid main-content-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary box-centered">
                        <div class="box-body">
                            <form class="form-main form-update" action="{{ url('/inquiry/warehouse/addw')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="nama_barang">Nama Barang</label>
                                    <input type="text" class="form-control" name="nama_barang" id="nama_barang" placeholder="Masukkan nama barang" required>
                                </div>
                                <div class="form-group">
                                    <label for="merk_barang">Merk Barang</label>
                                    <input type="text" class="form-control" name="merk_barang" id="merk_barang" placeholder="Masukkan Merk barang" required>
                                </div>
                                <div class="form-group">
                                    <label for="merk_barang">Kode</label>
                                    <input type="text" class="form-control" name="code" id="merk_barang" placeholder="Masukkan Merk barang" required>
                                </div>
                                <div class="form-group">
                                    <label for="jumlah">Harga</label>
                                    <input type="number" class="form-control" name="harga" id="harga" placeholder="Masukkan harga barang" min="1">
                                </div>
                                <div class="form-group">
                                    <label for="jumlah">Jumlah Barang</label>
                                    <input type="number" class="form-control" name="stok" id="jumlah" placeholder="Masukkan jumlah barang" min="1">
                                </div>
                                <a href="{{ url('/inquiry/warehouse') }}" class="btn btn-lg btn-danger btn-flat"><i class="fa fa-trash-o"></i>&nbsp; Batal</a>
                                <button type="submit" class="btn btn-lg btn-primary btn-flat"><i class="fa fa-save"></i>&nbsp; Simpan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
