@extends('adminlte::page')

@section('title', 'Kraken IT Solutions - Warehouse Database')

@section('content')
    <div class="content">
        <section class="content-header">
            <h1>
                Registered Warehouse<br>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('/') }}">
                        <i class="fa fa-dashboard"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="{{ url('/inquiry/warehouse') }}">
                        <i class="fa fa-file-text-o"></i> Warehouse List
                    </a>
                </li>
            </ol>
        </section>

        <section class="content container-fluid main-content-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><b>Warehouse List</b></h3>
                        </div>
                        <div class="box-body" style="padding: 10px 30px">
                            <div class="row">
                                <div class="col-md-12">
                                    @if(Auth::user()->role!='store')
                                    <a href="{{ url('/inquiry/warehouse/add') }}" class="btn btn-primary">Add an item</a>
                                    @endif
                                </div>
                            </div>
                            <br>
                            <!-- <hr style="border-style: dashed; border-width: 0.8px; border-color: gray"> -->
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-bordered table-striped table-hover" id="dataWarehouse" style="width: 100%">
                                        <thead>
                                        <tr>
                                            <td width="50">No.</td>
                                            <td><center>Nama Barang</center></td>
                                            <td><center>Merk</center></td>
                                            <td><center>Harga</center></td>
                                            <td><center>Jumlah Persediaan</center></td>
                                            <td><center>Aksi</center></td>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop

@section('js')
    <script type="text/javascript">

        var FormData;

        $(document).ready(function() {
            var tableLaporan = $('#dataWarehouse').DataTable({
                "sDom":"ltipr",
                "lengthMenu": [[10, 30, 100, 200, -1], [10, 30, 100, 200, "All"]],
                "scrollX": true,
                "scrollY": true,
                "language": {
                    "lengthMenu": "Tampil _MENU_ data per halaman",
                    "zeroRecords": "Tidak ada data yang ditemukan",
                    "info": "Halaman _PAGE_ dari _PAGES_",
                    "infoEmpty": "Data kosong",
                    "infoFiltered": "(difilter dari total _MAX_ data)",
                    "search": "Cari :",
                },
                "processing": true,
                "serverSide": true,
                "order": [],
                "ajax": {
                    "url": "<?= url('/inquiry/warehouse/table')?>",
                    "type": "GET",
                },
                "columnDefs": [
                    {
                        class: "text-center",
                        width: 30,
                        "targets": [0],
                        "orderable": false,
                        render: function(data, type, row, meta){
                            return meta.row+meta.settings._iDisplayStart+1
                        }
                    },
                    {
                        "targets": [1],
                        "data": "name",
                        width: 90,
                        "orderable": true,
                    },
                    {
                        class: "text-center",
                        "orderable": true,
                        "targets": [2],
                        "data": "merk"
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [3],
                        "data": "harga"
                    },
                    {
                        class: "text-center",
                        "orderable": false,
                        "targets": [4],
                        "data": "available_stock"
                    },
                    {
                        targets: [5],
                        "sortable": false,
                        "searchable": false,
                        render: function(data, type, row, meta){
                            return "<div class='btn-group'>"+
                                "<a href='{{url("/inquiry/warehouse/edit/")}}/"+row["id"]+"' class='btn btn-primary'>Edit</a>"+
                                "<a href='{{url("/inquiry/warehouse/delete/")}}/"+row["id"]+"' class='btn btn-danger' onclick='hapus()'>Delete</a>"+
                                "</div>";
                        }
                    }
                ],
            });

            tableLaporan.draw();
        });

        function hapus(){
            window.confirm('Dihapus?');
        }

    </script>
@stop
