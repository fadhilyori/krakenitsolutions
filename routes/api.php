<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('user')->group(function(){
	Route::get('/', 'Api\AuthController@users');
	Route::post('login', 'Api\AuthController@login');
	Route::post('register', 'Api\AuthController@register');
	Route::post('logout', 'Api\AuthController@logout');
});
Route::prefix('order')->group(function(){
	Route::get('{id}', 'Api\OrderController@list');
	Route::get('pdf/{id}', 'Api\OrderController@pdf');
});

Route::post('scan', 'Api\TrackSalesController@scan_qr');
Route::post('claim', 'Api\RecordSellingController@claim');
Route::post('report', 'Api\ActivityReportController@report');