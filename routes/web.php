<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');

Route::group(['prefix'=>'/'],function(){
    //REGISTERED USER
    
    //sales
    Route::get('/registered_user/sales',[
        "uses"=>"Web\RegisteredUserController@sales",
        "as"=>"salespage"
    ]);

    Route::get('/registered_user/sales/add', function () {
        return view('registered_user.addsales');
    });

    Route::post('/registered_user/sales/add',[
        "uses"=>"Web\RegisteredUserController@addsales",
        "as"=>"salespage"
    ]);

    Route::get('/registered_user/sales/table',[
        "uses"=>"Web\RegisteredUserController@salestable",
        "as"=>"salestable"
    ]);

    Route::get('/registered_user/sales/edit/{id}',[
        "uses"=>"Web\RegisteredUserController@editsales",
        "as"=>"saleseditpage"
    ]);

    Route::post('/registered_user/sales/update/{id}',[
        "uses"=>"Web\RegisteredUserController@storesales",
        "as"=>"postsalespage"
    ]);

    Route::get('/registered_user/sales/delete/{id}',[
        "uses"=>"Web\RegisteredUserController@deletesales",
        "as"=>"deletesales"
    ]);

    Route::get("/registered_user/sales/view/{id}",[
        "uses"=>"Web\RegisteredUserController@viewsales",
        "as"=>"viewsalespage"
    ]);

    Route::post("registered_user/sales/delete/{id}",[
        "uses"=>"Web\RegisteredUserController@deletesales",
        "as"=>"deletesales"
    ]);
    
    //admin
    Route::get('/registered_user/admin',[
        "uses"=>"Web\RegisteredUserController@admin",
        "as"=>"adminpage"
    ]);

    Route::get('/registered_user/admin/add', function () {
        return view('registered_user.addadmin');
    });

    Route::post('/registered_user/admin/add',[
        "uses"=>"Web\RegisteredUserController@addadmin",
        "as"=>"addadmin"
    ]);

    Route::get('/registered_user/admin/table',[
        "uses"=>"Web\RegisteredUserController@admintable",
        "as"=>"admintable"
    ]);

    Route::get("/registered_user/admin/view/{id}",[
        "uses"=>"Web\RegisteredUserController@viewadmin",
        "as"=>"viewadminpage"
    ]);

    Route::post("registered_user/admin/delete/{id}",[
        "uses"=>"Web\RegisteredUserController@deleteadmin",
        "as"=>"deleteadmin"
    ]);

    Route::get('/registered_user/admin/update/{id}',[
        "uses"=>"Web\RegisteredUserController@updateadmin",
        "as"=>"updateadminpage"
    ]);

    Route::post('/registered_user/admin/update/{id}',[
        "uses"=>"Web\RegisteredUserController@editadmin",
        "as"=>"editadmin"
    ]);

    //store
    Route::get('/registered_user/store',[
        "uses"=>"Web\RegisteredUserController@store",
        "as"=>"storepage"
    ]);

    Route::get('/registered_user/store/add', function () {
        return view('registered_user.addstore');
    });

    //tambahan
    Route::post('/registered_user/store/add', [
        "uses"=>"Web\RegisteredUserController@addstore",
        "as"=>"storetable"
    ]);
    //endtambahan

    Route::get('/registered_user/store/table',[
        "uses"=>"Web\RegisteredUserController@storetable",
        "as"=>"storetable"
    ]);

    Route::get("/registered_user/store/view/{id}",[
        "uses"=>"Web\RegisteredUserController@viewstore",
        "as"=>"viewstorepage"
    ]);

    Route::post("registered_user/store/delete/{id}",[
        "uses"=>"Web\RegisteredUserController@deletestore",
        "as"=>"deletestore"
    ]);

    Route::get('/registered_user/store/edit/{id}',[
        "uses"=>"Web\RegisteredUserController@editstore",
        "as"=>"updatestorepage"
    ]);

    Route::post('/registered_user/store/update/{id}',[
        "uses"=>"Web\RegisteredUserController@updatestore",
        "as"=>"editstore"
    ]);
    
    //END - REGISTERED USER
    
    //INQUIRY
    
    //order
    Route::get('/inquiry/order',[
        "uses"=>"Web\OrderController@order",
        "as"=>"orderpage"
    ]);

    Route::post('/inquiry/order/add',[
        "uses"=>"Web\OrderController@addOrder",
        "as"=>"addOrder"
    ]);

    Route::get('/inquiry/order/order_table',[
        "uses"=>"Web\OrderController@orderTable",
        "as"=>"orderTable"
    ]);

    Route::get('/inquiry/order/table/{id}',[
        "uses"=>"Web\OrderController@orderDetailTable",
        "as"=>"orderDetailTable"
    ]);

    Route::get("/inquiry/order/view/{id}",[
        "uses"=>"Web\OrderController@vieworder",
        "as"=>"vieworderpage"
    ]);

    Route::get("/inquiry/order/delete/{id}",[
        "uses"=>"Web\OrderController@deleteorder",
        "as"=>"deleteorder"
    ]);

    Route::post("/inquiry/order_detail/add",[
        "uses"=>"Web\OrderController@adddetailorder",
        "as"=>"deleteorderdetail"
    ]);

    Route::get("/inquiry/order_detail/delete/{id}",[
        "uses"=>"Web\OrderController@deleteorderdetail",
        "as"=>"deleteorderdetail"
    ]);

    Route::get('/inquiry/order/verify/{id}',[
        "uses"=>"Web\OrderController@verifyOrder",
        "as"=>"verifyorder"
    ]);
    
    Route::get('/inquiry/order/pdf/{id}',[
        "uses"=>"Web\OrderController@printpdf",
        "as"=>"printorder"
    ]);

    
    //claim
    //Route::get('/')
    Route::get('/inquiry/claim',[
        "uses"=>"Web\ClaimController@claim",
        "as"=>"claimpage"
    ]);

    Route::get('/inquiry/claim/table',[
        "uses"=>"Web\ClaimController@table",
        "as"=>"claimtable"
    ]);

    Route::get('/inquiry/claim/{id}',[
        "uses"=>"Web\ClaimController@viewclaim",
        "as"=>"viewclaim"
    ]);

    Route::get('/inquire/claim/delete/{id}',[
        "uses"=>"Web\ClaimController@destroy",
        "as"=>"destroyclaimpage"
    ]);

    Route::get('/inquiry/claim/pdf/{id}',[
        "uses"=>"Web\ClaimController@claimpdf",
        "as"=>"claimpdfpage"
    ]);

    Route::get('/inquiry/claim/add',[
        "uses"=>"Web\ClaimController@addclaim",
        "as"=>"addclaimpage"
    ]);
        
    Route::post('/inquiry/claim/save_claim',[
        "uses"=>"Web\ClaimController@saveclaim",
        "as"=>"saveclaimpage"
    ]);
        
    Route::get('/inquiry/claim/verify/{id}', [
        "uses"=>"Web\ClaimController@verifyclaim",
        "as"=>"verifyclaim"
    ]);

    //warehouse
    Route::get('/inquiry/warehouse', [
        "uses"=>"Web\WarehouseController@index",
        "as"=>"warehousepage"
    ]);

    Route::get('/inquiry/warehouse/table', [
        "uses"=>"Web\WarehouseController@warehousetable",
        "as"=>"warehousetable"
    ]);

    Route::get('/inquiry/warehouse/add', function () {
        return view('warehouse.addwarehouse');
    });

    Route::post('/inquiry/warehouse/addw',[
        "uses"=>"Web\WarehouseController@warehouseadd",
        "as"=>"warehousestorepost"
    ]);

    Route::get('/inquiry/warehouse/edit/{id}',[
        "uses"=>"Web\WarehouseController@edit",
        "as"=>"warehouseedit"
    ]);

    Route::post('/inquiry/warehouse/update/{id}',[
        "uses"=>"Web\WarehouseController@update",
        "as"=>"warehouseupdate"
    ]);

    Route::get('/inquiry/warehouse/delete/{id}',[
        "uses"=>"Web\WarehouseController@destroy",
        "as"=>"warehousedeleteget"
    ]);


    //END - INQUIRY

    //SALES

    //report
    Route::get('/sales/report/{id}',[
        "uses"=>"Web\SalesController@report",
        "as"=>"reportpage"
    ]);
    Route::get('/sales/report/table/{id}',[
        "uses"=>"Web\SalesController@reporttable",
        "as"=>"reporttable"
    ]);

    //daily_activity 
    Route::get('/sales/daily_activity/{id}',[
        "uses"=>"Web\SalesController@dailyactivity",
        "as"=>"dailypage"
    ]);

    Route::get('/sales/daily_activity/pdf/{id}',[
        "uses"=>"Web\SalesController@dailyactivitypdf",
        "as"=>"dailypdfpage"
    ]);

    Route::get('/sales/daily_activity/table/{id}',[
        "uses"=>"Web\SalesController@dailyactivitytable",
        "as"=>"dailytable"
    ]);

    Route::get('/qrcode', function () {
        return view('qrcode');
    });

    Route::get('/qr-code/generate/store/{id}', function ($id) {
        $store = App\Store::where('id', $id)->first();
        return QRCode::text($store)->setSize(8)->png();   
    });   
});

